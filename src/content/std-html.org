#+TITLE: Standard HTML for Authoring Experiment
#+AUTHOR: VLEAD
#+SETUPFILE: ../themes/readtheorg/org-templates/level-1.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
This document specifies the html syntax for authoring the
content of an experiment by the author.

** HTML5 format for authoring an experiment
The content of an experiment is authored in HTML5 (standard
html).  The syntax for experiment is described here.

** s-expression syntax for html
The standard html format for experiment is defined
below as a stylised html written in s-expression syntax.

* Notation
#+BEGIN_EXAMPLE
... : zero or more
?.. : zero or one
+.. : one or more
<nonterminal> : nonterminal in the grammar

#+END_EXAMPLE

* Grammar
** doc
#+BEGIN_SRC scheme
<doc> ::= <doc-type> <doc-content>
#+END_SRC

*** =<doc-type>=
#+BEGIN_SRC scheme
<doctype> ::= (!DOCTYPE html)
#+END_SRC

*** =<doc-content>=
The document body has experiment content (=<exp>=).
#+BEGIN_SRC scheme
<doc-content> ::=  (html
                    (head <attr-list> 
                    (title <attr-list>  <text>)
                    <meta> ...)
                    (body <attr-list> 
                    <exp>))

<meta> ::= meta elements
#+END_SRC

** Attributes
#+BEGIN_SRC scheme
<attr-list> ::= (<attr> ...) ; attributes may be listed in any order
<attr> ::= [<symbol> <str>]
#+END_SRC

** =<exp>=
An experiment has a mandatory short title which is the value
of the attribute title, which could be used in Table of
contents, for example.  The optional, longer title is the
text in the heading sub-element of in =<title-div>=.  An
experiment has a =<preamble>=, which contains a set of
artefacts. This preamble is then followed by zero or more
learning units(lu).

#+BEGIN_SRC scheme
<exp> ::= (div ([class "exp"]
                [title <title-str>]
                <attr> ...)
                <title-div> ?..
                <preamble>
                <LU> ...)
                
<title-div> ::= (div ([class "longt"] <attr> ...)
                 (<h>([class "hlongt"] <attr>...) <text>))

<h> ::= h1 | h2 | h3 | h4 | h5 | h6

<text> ::= html-text (Text in HTML)

<preamble> ::= (div ([class "preamble"] <attr> ...) <art> +..)
#+END_SRC

** =<lu>=
An =<lu>= has a mandatory short title which is the value of
the attribute title, which could be used in Table of
Contents, for example.  The optional, longer title is the
text in the heading sub-element of in =<title-div>=.  An
=<lu>= has a =<preamble>=, which contains a set of
artefacts. This preamble is then followed by zero or more
tasks and quizzes.

#+BEGIN_SRC scheme
<lu>  ::= (div ([class "lu"]
                [title <title-str>]
                <attr> ...)
                <title-div> ?..
                <preamble>
                <task> ...
                <quiz> ...)
#+END_SRC

** =<task>=
A =<task>= has a mandatory short title which is the value of
the attribute =title=, which could be used in Table of
Contents, for example.  The optional, longer title is the
text in the heading sub-element of in =<title-div>=.

The =<art>='s are preceded by an optional =<title-div>=
element.

#+BEGIN_SRC scheme
<task> ::= (div ([class "task"] 
                 [title <title-str>]
                 <attr> ...)
                 <title-div> ?..
                 <art> ...)
#+END_SRC

** =<art>=
*** Inline and Complete
**** Text
Text artefact element has a mandatory short title attribute
followed by an optional long one.  The optional
=<title-div>= is then followed by =<art-body>=.

The =<art-body>= has text artefact content.
#+BEGIN_SRC scheme
<txt-art> ::= (div ([class "art txt"]
                    [title <title-str>]
                    <attr> ...) 
                    <title-div> ?..
                    <art-body>)

<art-body> ::= (div ([class "art-body"] <attr> ...) <html> +..)

<html> ::= any html element (excluding the main html
           elements like html, body, head, etc.)  or text.
#+END_SRC

**** Image
Image artefact element has a mandatory short title attribute
followed by an optional long one. The optional =<title-div>=
is then followed by =<art-body>=.  The =<art-body>= has
image artefact content.  The =img= element following that
has two mandatory attributes: =url= and =alt=.  That is
followed by any number of =<html>= elements.

#+BEGIN_SRC scheme
<img-art> ::= (div ([class "art img"]
                    [title <title-str>]
                    <attr> ...)
                    <title-div> ?..
                    <art-body>)

<art-body> ::= (div ([class "art-body"] <attr> ...) <img>)

<img> ::= (img ([src <url-str>]
                [alt <alt-str>]
                <attr> ...)
                <html> ...)
#+END_SRC

**** Video
Video artefact element has a mandatory short title attribute
followed by an optional long one. The optional =<title-div>=
is then followed by =<art-body>=.  The =<art-body>= has
video artefact content.

#+BEGIN_SRC scheme
<video-art> ::= (div ([class "art video"]
                     [title <title-str>]
                     <attr> ...)
                     <title-div> ?..
                     <art-body>)

<art-body> ::= (div ([class "art-body"] <attr> ...) <html-video>))

<html-video> ::= (video <attr-list> <html> ...)
#+END_SRC

**** Audio
Audio artefact element has a mandatory short title attribute
followed by an optional long one. The optional =<title-div>=
is then followed by =<art-body>=.  The =<art-body>= has
video artefact content.

#+BEGIN_SRC scheme
<audio-art> ::= (div ([class "art audio"]
                     [title <title-str>]
                     <attr> ...)
                     <title-div> ?..
                     <art-body>)

<art-body> ::= (div ([class "art-body"] <attr> ...) <html-audio>))

<html-audio> ::= (audio <attr-list> <html> ...)
#+END_SRC

*** Requirement artefact
A =<req-art>= element has a mandatory short title attribute
followed by an optional long one. That is then followed by a
=description= which can have one or more html elements.

#+BEGIN_SRC scheme
<req-art> ::= (div ([class "art req"]
                    [title <title-str>]
                    <attr> ...)
                    <title-div> ?..
                    <description>)

<description> ::= (div ([class "description"] <attr> ...)
                    <html> +..)
#+END_SRC

** =<quiz>=
A =<quiz>= has a mandatory short title which is the value of
the attribute =title=, which could be used in Table of
Contents, for example.  The optional, longer title is the
text in the heading sub-element of in =<title-div>=. A quiz
has an optional =<preamble>=, which contains a set of
artefacts. This preamble is then followed by one or more
Multiple Choice Questions(mcq).
#+BEGIN_SRC scheme
<quiz> ::= (div ([class "quiz"] 
                 [title <title-str>]
                 <attr> ...)
                 <title-div> ?..
                 <preamble> ?..
                 <mcq> +..)
#+END_SRC

** =<mcq>=
A =<mcq>= has a =<preamble>=, which contains a set of
artefacts. This preamble is then followed by one or more
=<choices>= and =<explanation>=.
#+BEGIN_SRC scheme
<mcq> ::= (div ([class "mcq"] 
                 <attr> ...)
                 <preamble>
                 <choice> +..
                 <explanation> +..)
#+END_SRC

** =<choice>=
A =<choice>= has a attribute with a =<bool>= value which
represents the correct or wrong option, which could be used
in quiz for validation. A =<choice>= has a =<preamble>=,
which contains a set of artefacts.
#+BEGIN_SRC scheme
<choice> ::= (div ([class "choice"
                    data-correct <bool>] 
                    <attr> ...)
                    <preamble>)
                 ;; <correct?>)

<bool> ::= true | false
;; <correct?> ::= (div ([class "correct? right?"]))
;; <right?> ::= true | false
#+END_SRC
** =<explanation>=
A =<explanation>= has a =<preamble>=, which contains a set
of artefacts.
#+BEGIN_SRC scheme
<explanation> ::= (div ([class "explanation"] 
                        <attr> ...)
                        <preamble>)
#+END_SRC
